package com.tubanco.transferencias;

import java.util.Scanner;

public class GestorCuentas {

	private static CuentaService service = new CuentaService();
	private static Scanner scanner = new Scanner(System.in);

	public GestorCuentas() {

	}

	public static void main(String[] args) {
		int opcion = 0;
	

		while (opcion != 99) {
			System.out.println("**** MENU ****");
			System.out.println("1) Crear cuenta");
			System.out.println("2) Borrar cuenta");
			System.out.println("3) Listar cuentas");
			System.out.println("4) Transferencia");
			System.out.println("==============");
			System.out.print("Ingrese una opcion (salir=99): ");
			opcion = Integer.valueOf(scanner.nextLine());

			switch (opcion) {
			case 1:
				crearCuenta();
				break;
				
			case 2:
				borrarCuenta();
				break;
				
			case 3:
				listarCuentas();
				break;
				
			case 4:
				transferencia();
				break;

			default:
				break;
			}

		}

	}

	public static void crearCuenta() {
		int codigo;
		String cliente;
		double balance;

		System.out.println("Ingrese código: ");
		codigo = Integer.valueOf(scanner.nextLine());
		System.out.println("Ingrese balance: ");
		balance = Double.valueOf(scanner.nextLine());
		System.out.println("Ingrese cliente: ");
		cliente = scanner.nextLine();

		service.crearCuenta(codigo, balance, cliente);

	}

	public static void listarCuentas() {
		System.out.println("Listado de Cuentas:");
		System.out.println("===================");
		for (Cuenta cuenta : service.obtenerCuentas()) {
			System.out.println(cuenta);
		}

	}

	public static void borrarCuenta() {
		System.out.println("Ingrese código de la cuenta a borrar: ");
		int codigo = Integer.valueOf(scanner.nextLine());
		boolean resultado = service.borrarCuenta(codigo);
		if(resultado) {
			System.out.println("La cuenta " + codigo + " se ha borrado!");
		}else {
			System.out.println("No se pudo borrar la cuenta!");
		}
		
	}
	
	public static void transferencia() {
		int cuentaOrigen, cuentaDestino;
		double monto;
		
		System.out.println("Ingrese cuenta Origen: ");
		cuentaOrigen = Integer.valueOf(scanner.nextLine());
		System.out.println("Ingrese cuenta Destino: ");
		cuentaDestino = Integer.valueOf(scanner.nextLine());
		System.out.println("Ingrese monto a transferir: ");
		monto = Double.valueOf(scanner.nextLine());
		
		service.transferir(cuentaOrigen, cuentaDestino, monto);
	}
}
