package com.tubanco.transferencias;

public class Cuenta {

	private int codigo;
	private double balance;
	private String cliente;

	public Cuenta() {

	}

	public Cuenta(int codigo, double balance, String cliente) {
		this.codigo = codigo;
		this.balance = balance;
		this.cliente = cliente;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	
	@Override
	public String toString() {
		return "{codigo: " + codigo + ", balance:" + balance + ", cliente:" + cliente + "}";
	}
}
