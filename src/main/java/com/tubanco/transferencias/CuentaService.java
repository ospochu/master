package com.tubanco.transferencias;

import java.util.ArrayList;
import java.util.List;

public class CuentaService {

	List<Cuenta> cuentas = new ArrayList<>();

	public CuentaService() {

	}

	public CuentaService(List<Cuenta> cuentas) {
		this.cuentas = cuentas;
	}

	public boolean crearCuenta(int codigo, double balance, String cliente) {
		boolean resultado = false;
		if (buscarCuenta(codigo) == null) {
			cuentas.add(new Cuenta(codigo, balance, cliente));
			resultado = true;
		}
		return resultado;
	}

	public List<Cuenta> obtenerCuentas() {
		return this.cuentas;
	}

	public Cuenta buscarCuenta(int codigo) {
		Cuenta cuenta = null;
		for (Cuenta cuentaAux : cuentas) {
			if (cuentaAux.getCodigo() == codigo)
				cuenta = cuentaAux;
		}
		return cuenta;
	}

	public Cuenta debitar(Cuenta cuenta, double monto) {
		if (cuenta.getBalance() >= monto) {
			cuenta.setBalance(cuenta.getBalance() - monto);
		} else {
			//throw new RuntimeException("No tiene saldo suficiente");
			return null;
		}
		return cuenta;
	}

	public Cuenta abonar(Cuenta cuenta, double monto) {
		cuenta.setBalance(cuenta.getBalance() + monto);
		return cuenta;
	}

	public boolean borrarCuenta(int codigo) {
		boolean resultado = false;

		for (int i = 0; i < this.cuentas.size(); i++) {
			if (this.cuentas.get(i).getCodigo() == codigo) {
				this.cuentas.remove(i);
				resultado = true;
			}
		}

		return resultado;
	}

	public boolean transferir(int codOrigen, int codDestino, double monto) {
		boolean resultado = false;
		Cuenta origen = buscarCuenta(codOrigen);
		Cuenta destino = buscarCuenta(codDestino);

		if (origen == null || destino == null) {
			throw new RuntimeException("Código de la cuenta de origen y/o destino incorrectas");
		}

		try {
			origen = debitar(origen, monto);
			destino = abonar(destino, monto);
			resultado = true;
		} catch (RuntimeException ex) {
			resultado = false;
		}

		return resultado;
	}
}
