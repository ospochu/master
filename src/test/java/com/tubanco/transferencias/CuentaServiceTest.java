package com.tubanco.transferencias;

import static org.junit.Assert.*;

import org.junit.Test;

public class CuentaServiceTest {

	private boolean resultado;

	@Test
	public void testCrearCuentaOK() {
		CuentaService cuentaservice = new CuentaService();
		resultado = false;
		
		resultado = cuentaservice.crearCuenta(12345, 2000.00, "cliente1");
		Cuenta cuenta = cuentaservice.buscarCuenta(12345);
		assertEquals(cuenta.getCodigo(), 12345);
		assertEquals(resultado, true);
	}

	@Test
	public void testCrearCuentaDup() {
		CuentaService cuentaservice = new CuentaService();
		
		boolean resultado = cuentaservice.crearCuenta(12345, 2000.00, "cliente1");
		resultado = cuentaservice.crearCuenta(12345, 2000.00, "cliente1");		
		assertEquals(resultado, false);
	}
	
	@Test
	public void testBuscarCuentaOK() {
		CuentaService cuentaservice = new CuentaService();
		resultado = false;
		
		resultado = cuentaservice.crearCuenta(12345, 2000.00, "cliente1");
		Cuenta cuenta = cuentaservice.buscarCuenta(12345);
		assertEquals(cuenta.getCodigo(), 12345);
		assertEquals(resultado, true);		
	}
	
	@Test
	public void testBorrarCuentaOK() {
		CuentaService cuentaservice = new CuentaService();
		resultado = false;
		
		resultado = cuentaservice.crearCuenta(12345, 2000.00, "cliente1");
		resultado = cuentaservice.borrarCuenta(12345);
		assertEquals(resultado, true);
	}
	
	@Test
	public void testBorrarCuentaNoOK() {
		CuentaService cuentaservice = new CuentaService();
		resultado = false;
		
		resultado = cuentaservice.crearCuenta(12345, 2000.00, "cliente1");
		resultado = cuentaservice.borrarCuenta(99999);
		assertEquals(resultado, false);
	}
	
	@Test
	public void testTransferirCuentaOK(){
		CuentaService cuentaservice = new CuentaService();
		
		boolean resultado = cuentaservice.crearCuenta(12345, 2000.00, "cliente1");
		resultado = cuentaservice.crearCuenta(12346, 2000.00, "cliente1");
		resultado = cuentaservice.transferir(12345, 12346, 100);
		assertEquals(resultado, true);

	}

	@Test
	public void DebitarNoOK(){
		CuentaService cuentaservice = new CuentaService();
		
		resultado = cuentaservice.crearCuenta(12345, 2000.00, "cliente1");
		Cuenta cuenta = cuentaservice.buscarCuenta(12345);
		cuenta = cuentaservice.debitar(cuenta, 5000.00);
		assertEquals(cuenta,null);

	}
	
}
